package com.airline.repository;

import com.airline.dao.AirplaneDao;
import com.airline.entity.Airplane;
import com.airline.exception.DaoException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class AirplaneRepository {
    private AirplaneDao airplaneDao;

    private List<Airplane> airplanes;

    public AirplaneRepository(AirplaneDao airplaneDao) {
        this.airplaneDao = airplaneDao;
        this.airplanes = new ArrayList<>();
    }

    public int save(Airplane airplane) throws DaoException {
        int id = airplaneDao.save(airplane);
        airplanes.add(airplane);
        return id;
    }

    public int edit(Airplane airplane) throws DaoException {
        int id = airplaneDao.update(airplane);
        for (Airplane a : airplanes) {
            if (a.equals(airplane)) {
                a = airplane;
            }
        }
        return id;
    }

    public void delete(int id) throws DaoException {
        airplaneDao.delete(id);
        airplanes.removeIf(a -> a.getId() == id);
    }

    public Airplane getById(int id) throws DaoException {
        Airplane airplane = airplaneDao.getById(id);
        addAirplaneToListIfNotExist(airplane);
        return airplane;
    }

    public List<Airplane> getAllAirplanes() throws DaoException {
        return airplanes.addAll(airplaneDao.getAll()) ? airplanes : null;
    }

    public Airplane getAirplaneByFuelConsumption(double fuelConsumption) throws DaoException {
        Airplane airplane = airplaneDao.getAirplaneByFuelConsumption(fuelConsumption);
        addAirplaneToListIfNotExist(airplane);
        return airplane;
    }

    public List<Airplane> getAirplanesByFuelConsumption(double fuelConsumption) throws DaoException {
        List<Airplane> airplanesByFuelConsumption = airplaneDao.getAirplanesByFuelConsumption(fuelConsumption);
        addAirplanesToListIfNotExist(airplanesByFuelConsumption);
        return airplanesByFuelConsumption;
    }

    public int getSumOfAllAirplanesPassengerSeats() throws DaoException {
        int sumOfAllAirplanesPassengerSeatsFromDb = airplaneDao.getSumOfAllAirplanesPassengerSeats();
        int sumOfAllAirplanesPassengerSeatsFromList = 0;
        for (Airplane airplane : airplanes) {
            sumOfAllAirplanesPassengerSeatsFromList += airplane.getNumberOfPassengerSeats();
        }
        if(sumOfAllAirplanesPassengerSeatsFromDb != sumOfAllAirplanesPassengerSeatsFromList){
            airplanes = getAllAirplanes();
        }
        return sumOfAllAirplanesPassengerSeatsFromDb;
    }

    public double getSumOfAllAirplanesCarryingCapacity() throws DaoException{
        double sumOfAllAirplanesCarryingCapacityFromDb = airplaneDao.getSumOfAllAirplanesCarryingCapacity();
        double sumOfAllAirplanesCarryingCapacityFromList = 0.0;
        for (Airplane airplane : airplanes) {
            sumOfAllAirplanesCarryingCapacityFromList += airplane.getNumberOfPassengerSeats();
        }
        if(sumOfAllAirplanesCarryingCapacityFromDb != sumOfAllAirplanesCarryingCapacityFromList){
            airplanes = getAllAirplanes();
        }
        return sumOfAllAirplanesCarryingCapacityFromDb;
    }

    private void addAirplaneToListIfNotExist(Airplane airplane) {
        if (!airplanes.contains(airplane)) {
            airplanes.add(airplane);
        }
    }

    private void addAirplanesToListIfNotExist(Collection<Airplane> collection) {
        for (Airplane airplane : collection) {
            if (!airplanes.contains(airplane)) {
                airplanes.add(airplane);
            }
        }
    }
}
