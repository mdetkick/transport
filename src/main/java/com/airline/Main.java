package com.airline;

import com.airline.dao.AirplaneDao;
import com.airline.dao.MySqlAirplaneDao;
import com.airline.exception.DaoException;

public class Main {
    public static void main(String[] args) throws DaoException {
        AirplaneDao airplaneDao = new MySqlAirplaneDao();
        System.out.println(airplaneDao.getSumOfAllAirplanesPassengerSeats());
    }
}
