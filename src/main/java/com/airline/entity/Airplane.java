package com.airline.entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Airplane {

    private static Logger LOGGER = LogManager.getLogger(Airplane.class);

    private int id;
    private String name;
    private double rangeOfFlight;
    private double fuelConsumption;
    private double cost;
    private int numberOfPassengerSeats;
    private double carryingCapacity;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        if (name == null || name.isEmpty()) {
            LOGGER.debug("name = null or empty");
            throw new IllegalArgumentException();
        }
        this.name = name;
    }

    public void setRangeOfFlight(double rangeOfFlight) {
        if (rangeOfFlight <= 0) {
            LOGGER.debug("range of flight <= 0");
            throw new IllegalArgumentException();
        }
        this.rangeOfFlight = rangeOfFlight;
    }

    public void setFuelConsumption(double fuelConsumption) {
        if (fuelConsumption <= 0.0) {
            LOGGER.debug("fuel consumption <= 0.0");
            throw new IllegalArgumentException();
        }
        this.fuelConsumption = fuelConsumption;
    }

    public void setCost(double cost) {
        if (cost <= 0.0) {
            LOGGER.debug("cost <= 0");
            throw new IllegalArgumentException();
        }
        this.cost = cost;
    }

    public void setNumberOfPassengerSeats(int numberOfPassengerSeats) {
        if (numberOfPassengerSeats <= 0) {
            LOGGER.debug("number of passenger seats <= 0");
            throw new IllegalArgumentException();
        }
        this.numberOfPassengerSeats = numberOfPassengerSeats;
    }

    public void setCarryingCapacity(double carryingCapacity) {
        if (carryingCapacity <= 0) {
            LOGGER.debug("carrying capacity <= 0");
            throw new IllegalArgumentException();
        }
        this.carryingCapacity = carryingCapacity;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getRangeOfFlight() {
        return rangeOfFlight;
    }

    public double getFuelConsumption() {
        return fuelConsumption;
    }

    public double getCost() {
        return cost;
    }

    public int getNumberOfPassengerSeats() {
        return numberOfPassengerSeats;
    }

    public double getCarryingCapacity() {
        return carryingCapacity;
    }

    public Airplane() {
    }

    public Airplane(String name, double rangeOfFlight, double fuelConsumption, double cost, int numberOfPassengerSeats, double carryingCapacity) {
        this.name = name;
        this.rangeOfFlight = rangeOfFlight;
        this.fuelConsumption = fuelConsumption;
        this.cost = cost;
        this.numberOfPassengerSeats = numberOfPassengerSeats;
        this.carryingCapacity = carryingCapacity;
    }

    @Override
    public int hashCode() {
        final int prime = 45;
        int result = 1;
        result = prime * result + id;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + Double.hashCode(rangeOfFlight);
        result = prime * result + Double.hashCode(fuelConsumption);
        result = prime * result + Double.hashCode(cost);
        result = prime * result + numberOfPassengerSeats;
        result = prime * result + Double.hashCode(carryingCapacity);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Airplane)) {
            return false;
        }
        Airplane airplane = (Airplane) obj;
        return this.id == airplane.id && this.name.equals(airplane.name)
                && this.rangeOfFlight == airplane.rangeOfFlight && this.fuelConsumption == airplane.fuelConsumption
                && this.cost == airplane.cost && this.numberOfPassengerSeats == airplane.numberOfPassengerSeats
                && this.carryingCapacity == this.carryingCapacity;
    }

    @Override
    public String toString() {
        return "Airplane{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", rangeOfFlight=" + rangeOfFlight +
                ", fuelConsumption=" + fuelConsumption +
                ", cost=" + cost +
                ", numberOfPassengerSeats=" + numberOfPassengerSeats +
                ", carryingCapacity=" + carryingCapacity +
                '}';
    }
}
