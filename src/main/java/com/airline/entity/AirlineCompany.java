package com.airline.entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class AirlineCompany {

    private static Logger LOGGER = LogManager.getLogger(AirlineCompany.class);

    private String name;
    private List<Airplane> airplanes;
    private static AirlineCompany airlineCompany;

    public void setName(String name) {
        if(name == null || name.isEmpty()){
            LOGGER.debug("name = null or empty");
            throw new IllegalArgumentException();
        }
        this.name = name;
    }

    public void setAirplanes(List<Airplane> airplanes) {
        if(airplanes == null || airplanes.size()==0){
            LOGGER.debug("airplanes list = null or empty");
            throw new IllegalArgumentException();
        }
        this.airplanes = airplanes;
    }

    public String getName() {
        return name;
    }

    public List<Airplane> getAirplanes() {
        return airplanes;
    }

    public static AirlineCompany getAirlineCompany() {
        if (airlineCompany == null) {
            airlineCompany = new AirlineCompany();
        }
        return airlineCompany;
    }

    private AirlineCompany() {
        this.airplanes = new ArrayList<Airplane>();
    }
}
