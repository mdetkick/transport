package com.airline.enums;

public enum DbDrivers {
    MYSQL("");

    private String driver;

    DbDrivers(String driver){
        this.driver = driver;
    }

    public String getDriver() {
        return driver;
    }
}
