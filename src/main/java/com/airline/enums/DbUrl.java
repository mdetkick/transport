package com.airline.enums;

public enum DbUrl {
    MYSQL("jdbc:mysql://localhost:3306/airlinecompany?useTimezone=true&serverTimezone=UTC");

    private String url;

    DbUrl(String url){
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
