package com.airline.enums;

public enum DbPassword {
    MYSQL("TteoDTQM");

    private String password;

    DbPassword(String password){
        this.password = password;
    }

    public String getPassword() {
        return password;
    }
}
