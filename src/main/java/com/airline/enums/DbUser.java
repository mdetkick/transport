package com.airline.enums;

public enum DbUser {
    MYSQL("root");

    private String user;

    DbUser(String user){
        this.user = user;
    }

    public String getUser() {
        return user;
    }
}
