package com.airline.dao;

import com.airline.entity.Airplane;
import com.airline.exception.DaoException;

import java.sql.Connection;
import java.util.List;

public interface GenericDao<T> {
    int save(T t) throws DaoException;

    T getById(int id) throws DaoException;

    List<T> getAll() throws DaoException;

    void delete(int id) throws DaoException;

    int update(T t) throws DaoException;
}
