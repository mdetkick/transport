package com.airline.dao.factory;

import com.airline.dao.AirplaneDao;
import com.airline.dao.GenericDao;
import com.airline.dao.MySqlAirplaneDao;
import com.airline.entity.Airplane;

public class MySqlAirlineDaoFactory extends AirplaneDaoFactory {
    @Override
    public AirplaneDao getAirplaneDao() {
        return new MySqlAirplaneDao();
    }
}
