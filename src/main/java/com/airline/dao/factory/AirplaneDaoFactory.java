package com.airline.dao.factory;

import com.airline.dao.AirplaneDao;

public abstract class AirplaneDaoFactory {
    public static final int MYSQL = 1;

    public abstract AirplaneDao getAirplaneDao();

    public static AirplaneDaoFactory getDaoFactory(int factory) {
        AirplaneDaoFactory airlineDaoFactory = null;
        switch (factory) {
            case MYSQL:
                airlineDaoFactory = new MySqlAirlineDaoFactory();
        }
        return airlineDaoFactory;
    }
}
