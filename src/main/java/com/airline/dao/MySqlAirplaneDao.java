package com.airline.dao;

import com.airline.entity.Airplane;
import com.airline.exception.DaoException;
import com.airline.utils.MySqlUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySqlAirplaneDao extends MySqlUtil implements AirplaneDao {

    private static final Logger LOGGER = LogManager.getLogger(MySqlAirplaneDao.class);

    @Override
    public int save(Airplane airplane) throws DaoException {
        Connection connection = openConnection();
        PreparedStatement preparedStatement = null;
        String sql = "INSERT INTO airplane( name, range_of_flight, fuel_consumption, cost, passenger_seats, carrying_capacity) \n" +
                "VALUES ( ?, ?, ?, ?, ?, ?)";
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, airplane.getName());
            preparedStatement.setDouble(2, airplane.getRangeOfFlight());
            preparedStatement.setDouble(3, airplane.getFuelConsumption());
            preparedStatement.setDouble(4, airplane.getCost());
            preparedStatement.setInt(5, airplane.getNumberOfPassengerSeats());
            preparedStatement.setDouble(6, airplane.getCarryingCapacity());
            int row = preparedStatement.executeUpdate();
            if (row == 0) {
                throw new DaoException("Creating user failed.");
            }
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                airplane.setId(resultSet.getInt(1));
                LOGGER.info("Airplane was saved: " + airplane);
            }
        } catch (SQLException e) {
            LOGGER.error("Cant save airplane cause: {}", e.getMessage());
            throw new DaoException("Cant' save airplane", e);
        } finally {
            closeResultSet(resultSet);
            closePreparedStatement(preparedStatement);
            closeConnection(connection);
        }
        return airplane.getId();
    }

    @Override
    public Airplane getById(int id) throws DaoException {
        Connection connection = openConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        String sql = "SELECT name, range_of_flight, fuel_consumption, cost, passenger_seats, carrying_capacity FROM airplane WHERE idairplane=?";
        Airplane airplane = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                airplane = new Airplane();
                airplane.setId(id);
                airplane.setName(resultSet.getString("name"));
                airplane.setRangeOfFlight(resultSet.getDouble("range_of_flight"));
                airplane.setFuelConsumption(resultSet.getDouble("fuel_consumption"));
                airplane.setCost(resultSet.getDouble("cost"));
                airplane.setNumberOfPassengerSeats(resultSet.getInt("passenger_seats"));
                airplane.setCarryingCapacity(resultSet.getDouble("carrying_capacity"));
                LOGGER.info("DB return airplane with id : {}", id);
            } else {
                LOGGER.warn("No such airplane in DB with id : {}", id);
            }
        } catch (SQLException e) {
            LOGGER.error("Cant get airplane cause : {}", e.getMessage());
            throw new DaoException("Cant get airplane", e);
        } finally {
            closeResultSet(resultSet);
            closePreparedStatement(preparedStatement);
            closeConnection(connection);
        }
        return airplane;
    }

    @Override
    public List<Airplane> getAll() throws DaoException {
        Connection connection = openConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Airplane> airplanes = new ArrayList<>();
        String sql = "SELECT * FROM airplane";
        Airplane airplane = null;
        int recordsInTable;
        try {
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                airplane = new Airplane();
                airplane.setId(resultSet.getInt("idairplane"));
                airplane.setName(resultSet.getString("name"));
                airplane.setRangeOfFlight(resultSet.getDouble("range_of_flight"));
                airplane.setFuelConsumption(resultSet.getDouble("fuel_consumption"));
                airplane.setCost(resultSet.getDouble("cost"));
                airplane.setNumberOfPassengerSeats(resultSet.getInt("passenger_seats"));
                airplane.setCarryingCapacity(resultSet.getDouble("carrying_capacity"));
                airplanes.add(airplane);
            }
            recordsInTable = airplanes.size();
            if (recordsInTable > 0) {
                LOGGER.info("Table has : {} records", recordsInTable);
            } else {
                LOGGER.warn("No records in table");
            }
        } catch (SQLException e) {
            LOGGER.error("Cant get records cause : {}", e.getMessage());
            throw new DaoException("Cant get records", e);
        } finally {
            closeResultSet(resultSet);
            closePreparedStatement(preparedStatement);
            closeConnection(connection);
        }
        return airplanes;
    }

    @Override
    public void delete(int id) throws DaoException {
        Connection connection = openConnection();
        PreparedStatement preparedStatement = null;
        String sql = "DELETE FROM airplane WHERE idairplane = ?";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            if (preparedStatement.executeUpdate() == 1) {
                LOGGER.info("Airplane with id : {} was removed", id);
            } else {
                LOGGER.warn("Airplane with id : {} doesn't exist", id);
            }
        } catch (SQLException e) {
            LOGGER.error("Airplane with id : {} wasn't removed cause : {}", id, e.getMessage());
            throw new DaoException("Airplane wasn't removed", e);
        } finally {
            closePreparedStatement(preparedStatement);
            closeConnection(connection);
        }
    }

    @Override
    public int update(Airplane airplane) throws DaoException {
        int airplaneID = airplane.getId();
        Connection connection = openConnection();
        PreparedStatement preparedStatement = null;
        String sql = "UPDATE airplane SET name = ?, range_of_flight = ?, fuel_consumption = ?, cost = ?, passenger_seats = ?, carrying_capacity = ? WHERE idairplane = ?";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, airplaneID);
            preparedStatement.setString(2, airplane.getName());
            preparedStatement.setDouble(3, airplane.getRangeOfFlight());
            preparedStatement.setDouble(4, airplane.getFuelConsumption());
            preparedStatement.setDouble(5, airplane.getCost());
            preparedStatement.setInt(6, airplane.getNumberOfPassengerSeats());
            preparedStatement.setDouble(7, airplane.getCarryingCapacity());
            if (preparedStatement.executeUpdate() == 1) {
                LOGGER.info("Airplane with id : {} was edited", airplaneID);
            } else {
                LOGGER.warn("Airplane with id : {} doesnt exist", airplaneID);
            }
        } catch (SQLException e) {
            LOGGER.error("Can't edit airplane with id : {} cause : {}", airplaneID, e.getMessage());
            throw new DaoException("Can't edit airplane", e);
        } finally {
            closePreparedStatement(preparedStatement);
            closeConnection(connection);
        }
        return airplane.getId();
    }

    @Override
    public Airplane getAirplaneByFuelConsumption(double fuelConsumption) throws DaoException {
        Connection connection = openConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        String sql = "SELECT idairplane, name, range_of_flight, cost, passenger_seats, carrying_capacity FROM airplane WHERE fuel_consumption=?";
        Airplane airplane = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setDouble(1, fuelConsumption);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                airplane = new Airplane();
                airplane.setId(resultSet.getInt("idairplane"));
                airplane.setName(resultSet.getString("name"));
                airplane.setRangeOfFlight(resultSet.getDouble("range_of_flight"));
                airplane.setFuelConsumption(fuelConsumption);
                airplane.setCost(resultSet.getDouble("cost"));
                airplane.setNumberOfPassengerSeats(resultSet.getInt("passenger_seats"));
                airplane.setCarryingCapacity(resultSet.getDouble("carrying_capacity"));
                LOGGER.info("DB return airplane with fuel consumption : {}", fuelConsumption);
            }
        } catch (SQLException e) {
            LOGGER.error("Can't get airplane with fuel consumption : {}, cause : {}", fuelConsumption, e.getMessage());
            throw new DaoException("Can't get airplane with fuel consumption", e);
        } finally {
            closeResultSet(resultSet);
            closePreparedStatement(preparedStatement);
            closeConnection(connection);
        }
        return airplane;
    }

    @Override
    public List<Airplane> getAirplanesByFuelConsumption(double fuelConsumption) throws DaoException {
        Connection connection = openConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        String sql = "SELECT idairplane, name, range_of_flight, cost, passenger_seats, carrying_capacity FROM airplane WHERE fuel_consumption=?";
        Airplane airplane = null;
        List<Airplane> airplanes = new ArrayList<>();
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setDouble(1, fuelConsumption);
            resultSet = preparedStatement.executeQuery();
            int recordsInTable = 0;
            while (resultSet.next()) {
                airplane = new Airplane();
                airplane.setId(resultSet.getInt("idairplane"));
                airplane.setName(resultSet.getString("name"));
                airplane.setRangeOfFlight(resultSet.getDouble("range_of_flight"));
                airplane.setFuelConsumption(fuelConsumption);
                airplane.setCost(resultSet.getDouble("cost"));
                airplane.setNumberOfPassengerSeats(resultSet.getInt("passenger_seats"));
                airplane.setCarryingCapacity(resultSet.getDouble("carrying_capacity"));
                airplanes.add(airplane);
            }
            recordsInTable = airplanes.size();
            if (recordsInTable > 0) {
                LOGGER.info("Table has : {} records in table with fuel consumption : {}", recordsInTable, fuelConsumption);
            } else {
                LOGGER.warn("No records in table");
            }
        } catch (SQLException e) {
            LOGGER.error("Can't get airplanes with fuel consumption {},cause : {}", fuelConsumption, e.getMessage());
            throw new DaoException("Can't get airplanes with fuel consumption", e);
        } finally {
            closeResultSet(resultSet);
            closePreparedStatement(preparedStatement);
            closeConnection(connection);
        }
        return airplanes;
    }

    @Override
    public int getSumOfAllAirplanesPassengerSeats() throws DaoException {
        int sumOfAllAirplanesPassengerSeats = 0;
        Connection connection = openConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String sql ="SELECT SUM(passenger_seats) FROM airplane";
        try{
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            if(resultSet.next()){
                sumOfAllAirplanesPassengerSeats = resultSet.getInt("SUM(passenger_seats)");
                LOGGER.info("Get sum of passenger seats");
            }
        } catch (SQLException e) {
            LOGGER.error("Cant get sum of passenger seats cause:{}",e.getMessage());
        }
        return sumOfAllAirplanesPassengerSeats;
    }

    @Override
    public double getSumOfAllAirplanesCarryingCapacity() throws DaoException {
        double sumOfAllAirplanesCarryingCapacity = 0;
        Connection connection = openConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String sql ="SELECT SUM(carrying_capacity) FROM airplane";
        try{
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            if(resultSet.next()){
                sumOfAllAirplanesCarryingCapacity = resultSet.getInt("SUM(passenger_seats)");
                LOGGER.info("Get sum of passenger seats");
            }
        } catch (SQLException e) {
            LOGGER.error("Cant get sum of passenger seats cause:{}",e.getMessage());
        }
        return sumOfAllAirplanesCarryingCapacity;
    }
}
