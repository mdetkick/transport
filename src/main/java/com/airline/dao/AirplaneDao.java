package com.airline.dao;

import com.airline.entity.Airplane;
import com.airline.exception.DaoException;

import java.util.List;

public interface AirplaneDao extends GenericDao<Airplane> {
    Airplane getAirplaneByFuelConsumption(double fuelConsumption) throws DaoException;

    List<Airplane> getAirplanesByFuelConsumption(double fuelConsumption) throws DaoException;

    int getSumOfAllAirplanesPassengerSeats() throws DaoException;

    double getSumOfAllAirplanesCarryingCapacity() throws DaoException;
}
