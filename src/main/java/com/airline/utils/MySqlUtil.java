package com.airline.utils;

import com.airline.enums.DbDrivers;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;

public class MySqlUtil extends DbUtil {

    private static Logger LOGGER = LogManager.getLogger(MySqlUtil.class);

    private static final String MYSQL_DB_Driver = "com.mysql.cj.jdbc.Driver";
    private static final String MYSQL_DB_URL = "jdbc:mysql://localhost:3306/airlinecompany?useTimezone=true&serverTimezone=UTC";
    private static final String MYSQL_DB_USERNAME = "root";
    private static final String MYSQL_DB_PASSWORD = "TteoDTQM";

    @Override
    public Connection openConnection() {
        Connection connection = null;
        try {
            Class.forName(MYSQL_DB_Driver);
            connection = DriverManager.getConnection(MYSQL_DB_URL, MYSQL_DB_USERNAME, MYSQL_DB_PASSWORD);
            LOGGER.info("Connection is enabled : " + connection.getCatalog());
        } catch (ClassNotFoundException | SQLException e) {
            LOGGER.fatal("Connection isn't enabled cause : {}",e.getMessage());
        }
        return connection;
    }
}
