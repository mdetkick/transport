package com.airline.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;

public abstract class DbUtil {
    private static final Logger LOGGER = LogManager.getLogger(DbUtil.class);

    public abstract Connection openConnection();

    public void closeConnection(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
                LOGGER.info("Connection was closed");
            } catch (SQLException e) {
                LOGGER.error("Connection wasn't closed cause : {}", e.getMessage());
            }
        }
    }

    public void closePreparedStatement(PreparedStatement preparedStatement) {
        if (preparedStatement != null) {
            try {
                preparedStatement.close();
                LOGGER.info("Prepared statement was closed");
            } catch (SQLException e) {
                LOGGER.error("Cant close prepared statement cause : {}", e.getMessage());
            }
        }
    }

    public void closeResultSet(ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
                LOGGER.info("Result set was closed");
            } catch (SQLException e) {
                LOGGER.error("Cant close Result set cause {}", e.getMessage());
            }
        }
    }
}
